<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

  <title>Form Nilai Siswa</title>
</head>
<body>
  <h1 class="text-center mt-5">Form Nilai Siswa</h1>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-5 border mt-3 p-3">
        <form action="hasil.php" method="POST">
          <div class="mb-4">
            <input class="form-control" type="text" name="nama" placeholder="Nama" aria-label="default input example" >
          </div>
          <div class="form-group mb-4">
            <select name="mapel" class="form-control">
              <option selected>Mata Pelajaran</option>
              <option>Matematika</option>
              <option>IPA</option>
              <option>IPS</option>
              <option>PKN</option>
              <option>Penjaskes</option>
              <option>Agama</option>
            </select>
          </div>
          <div class="mb-4">
            <input class="form-control" type="number" min="1" max="100" name="UTS" placeholder="Nilai UTS" aria-label="default input example">
          </div>
          <div class="mb-4">
            <input class="form-control" type="number" min="1" max="100" name="UAS" placeholder="Nilai UAS" aria-label="default input example">
          </div>
          <div class="mb-4">
            <input class="form-control" type="number" min="1" max="100" name="tugas" placeholder="Nilai Tugas" aria-label="default input example">
          </div>
          <button type="submit" class="btn btn-primary">Simpan</button>

        </form>
      </div>
    </div>
  </div>




  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>


</body>

</html>